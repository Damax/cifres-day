import { createRouter, createWebHashHistory } from 'vue-router'

const HomeView = () => import('/src/pages/Home.vue')
const EvalView = () => import('/src/pages/Eval.vue')

const routes = [
    {
        path: '',
        component: HomeView,
        name: 'home',
    },
    {
        path: '/eval',
        component: EvalView,
        name: 'eval',
    },
]

export const router = createRouter({
    history: createWebHashHistory(),
    routes,
})
