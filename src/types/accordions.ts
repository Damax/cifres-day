export interface AccordionsItemProps {
    open: boolean
}

export interface Form {
    title: string,
    link: string
}

export interface Activity {
    summary: string
    content: string
    forms: Form[]
}